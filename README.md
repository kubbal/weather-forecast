# Weather Forecast Application

## The motivation

This is an enhanced version of one of my previous interview tasks. Back then I had to do a similar thing in python,
now I implemented it using Spring Boot, Hibernate, Spring JPA, Postgres, Jsoup, Maven and Java.

## The task

Parse the page of https://www.idokep.hu/idojaras/Budapest
to get the maximal and minimal temperature of the upcoming days.

I connected it with a Postgres database, so you can generate the parsed data
into it. Beyond that you can also do the regular CRUD operations.

As you have the chance to generate and store the weather forecast every day,
once you have reached the day you can look back and see how accurate the forecast was
by using this API and its custom search.

An example request to see what was the weather forecast on 27th of April 2022 for 28th of April 2022 (if you had such records in your database):
http://localhost:8080/api/v1/main/database/reports?reported=2022-04-27&date=2022-04-28

## Endpoints
| Method | URL | Required Parameters | Action |
|--------|-----|---------------------|--------|
| GET | /api/v1/main/max | None | Parses the forecast.url for the maximum temperature of the upcoming days. Result is returned as a JSON without database interaction. |
| GET | /api/v1/main/min | None | Parses the forecast.url for the minimum temperature of the upcoming days. Result is returned as a JSON without database interaction. |
| GET | /api/v1/main/all | None | Parses the forecast.url for the maximum and minimum temperatures of the upcoming days. Result is returned as a JSON without database interaction. |
| POST | /api/v1/main/database/create | A Report in the request body (2 dates, maximal and minimal temperature) | Saves new Report instance into the database |
| GET | /api/v1/main/database/reports | Optionally you can search for specific reported and date fields. Example: /api/v1/main/database/reports?reported=2022-04-27&date=2022-05-11 | Gets all existing or all specific reports from the database |
| GET | /api/v1/main/database/report/:id | ID | Retrieves a Report by its ID |
| PUT | /api/v1/main/database/report/:id | ID and Report | Updates a Report by its ID |
| DELETE | /api/v1/main/database/report/:id | ID | Deletes a Report from database by its ID |
| DELETE | /api/v1/main/database/clear | None | Deletes all Reports from the database |
| GET | /api/v1/main/database/generate | None | Fills database with forecasts from forecast.url |