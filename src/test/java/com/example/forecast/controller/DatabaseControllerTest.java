package com.example.forecast.controller;

import com.example.forecast.model.Report;
import com.example.forecast.repository.ReportRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;

import static org.mockito.Mockito.when;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc
class DatabaseControllerTest {

    private static final String CREATE_ENDPOINT = "/api/v1/main/database/create";
    private static final String GET_SECOND_ENDPOINT = "/api/v1/main/database/report/2";

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ReportRepository reportRepository;

    @MockBean
    private DatabaseController databaseController;

    private static final LocalDate timeNow = LocalDate.now();
    private static final int mockedMax = 25;
    private static final int mockedMin = 5;
    private static final Report mockedReport = new Report(timeNow, timeNow, mockedMax, mockedMin);
    private static final String mockedReportJson = "{" +
            "\"reportedOn\": \"" + timeNow + "\"," +
            "\"date\": \"" + timeNow + "\"," +
            "\"maximum\": " + mockedMax + "," +
            "\"minimum\": " + mockedMin + "}";

    @Test
    void getReportById() throws Exception {
        when(reportRepository.getById(2L)).thenReturn(mockedReport);

        MockHttpServletResponse response = mvc.perform(MockMvcRequestBuilders.get(GET_SECOND_ENDPOINT)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        assertEquals(HttpStatus.OK.value(), response.getStatus());

        when(databaseController.getReportById(253L)).thenReturn(new ResponseEntity<>(HttpStatus.NOT_FOUND));

        MockHttpServletResponse responseForInvalid = mvc.perform(MockMvcRequestBuilders.get("/api/v1/main/database/report/253")
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        assertEquals(HttpStatus.NOT_FOUND.value(), responseForInvalid.getStatus());
    }

    @Test
    void createReport() throws Exception {
        when(reportRepository.save(mockedReport)).thenReturn(mockedReport);
        mvc.perform(MockMvcRequestBuilders.post(CREATE_ENDPOINT)
                .accept(MediaType.APPLICATION_JSON)
                .content(mockedReportJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}