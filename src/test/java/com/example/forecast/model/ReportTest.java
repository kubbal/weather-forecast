package com.example.forecast.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class ReportTest {
    private LocalDate ld;
    private Report testReport;

    @BeforeEach
    void set() {
        ld = LocalDate.of(2020, 10, 14);
        testReport = new Report(ld, ld, 15, 13);
    }

    // ID is filled automatically by database
    @Test
    void getId() {
        Report report = new Report();
        assertEquals(null, report.getId());
    }

    @Test
    void getReportedOn() {
        assertEquals(ld, testReport.getReportedOn());

        LocalDate ld2 = LocalDate.of(2030, 10, 14);
        testReport.setReportedOn(ld2);
        assertEquals(ld2, testReport.getReportedOn());

        testReport.setReportedOn(null);
        assertEquals(null, testReport.getReportedOn());
    }

    @Test
    void getDate() {
        assertEquals(ld, testReport.getDate());

        LocalDate ld2 = LocalDate.of(2030, 10, 14);
        testReport.setDate(ld2);
        assertEquals(ld2, testReport.getDate());

        testReport.setDate(null);
        assertEquals(null, testReport.getDate());
    }

    @Test
    void getMaximum() {
        assertEquals(15, testReport.getMaximum());

        testReport.setMaximum(100);
        assertEquals(100, testReport.getMaximum());
    }

    @Test
    void getMinimum() {
        assertEquals(13, testReport.getMinimum());

        testReport.setMinimum(100);
        assertEquals(100, testReport.getMinimum());
    }
}