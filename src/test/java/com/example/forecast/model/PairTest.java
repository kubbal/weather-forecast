package com.example.forecast.model;

import org.junit.jupiter.api.Test;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class PairTest {

    @Test
    void getKey() {
        Pair<String, String> sp = new Pair<>("test1", "test2");
        assertEquals("test1", sp.getKey());
    }

    @Test
    void getValue() {
        Pair<Integer, Double> pid = new Pair<>(5, 2.37);
        assertEquals(2.37, pid.getValue());
    }

    @Test
    void testEquals() {
        Pair<Integer, String> pis = new Pair<>(2, "tt");
        Pair<String, Integer> psi = new Pair<>("tt", 2);
        Pair<Integer, String> pis2 = new Pair<>(2, "tt");
        assertNotEquals(pis, psi);
        assertEquals(pis, pis2);
        assertNotEquals(null, psi);
        assertEquals(pis, pis);
    }

    @Test
    void testHashCode() {
        Pair<String, String> ssp = new Pair<>("key", "value");
        assertEquals(Objects.hash(ssp.getKey(), ssp.getValue()),
                ssp.hashCode());
    }

    @Test
    void testToString() {
        Pair<Integer, Integer> iip = new Pair<>(0, 1);
        assertEquals(String.format("Pair{key=%s, value=%s}", iip.getKey(), iip.getValue()),
                iip.toString());
    }
}