function relocateMin() {
    window.location.href = "/api/v1/main/min";
}

function relocateMax() {
    window.location.href = "/api/v1/main/max";
}

function relocateAll() {
    window.location.href = "/api/v1/main/all";
}

function relocateToGenerate() {
    window.location.href = "/api/v1/main/database/generate"
}