package com.example.forecast.service;

import com.example.forecast.parser.WeatherParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Map;

@Service
public class MainService {

    @Autowired
    private WeatherParser weatherParser;

    public Map<LocalDate, Integer> getMaxTemperatures() {
        return weatherParser.collectMaxTemperatures();
    }

    public Map<LocalDate, Integer> getMinTemperatures() {
        return weatherParser.collectMinTemperatures();
    }

    public Map<LocalDate, Map<String, Integer>> getAll() {
        return weatherParser.collectAll();
    }

}
