package com.example.forecast.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
public class Report {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private LocalDate reportedOn;

    private LocalDate date;

    private int maximum;

    private int minimum;

    public Report(LocalDate reportedOn, LocalDate date, int maximum, int minimum) {
        this.reportedOn = reportedOn;
        this.date = date;
        this.maximum = maximum;
        this.minimum = minimum;
    }

    public Report() {}

    public Long getId() {
        return id;
    }

    public LocalDate getReportedOn() {
        return reportedOn;
    }

    public LocalDate getDate() {
        return date;
    }

    public int getMaximum() {
        return maximum;
    }

    public int getMinimum() {
        return minimum;
    }

    public void setReportedOn(LocalDate reportedOn) {
        this.reportedOn = reportedOn;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public void setMaximum(int maximum) {
        this.maximum = maximum;
    }

    public void setMinimum(int minimum) {
        this.minimum = minimum;
    }
}
