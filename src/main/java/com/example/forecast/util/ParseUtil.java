package com.example.forecast.util;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class ParseUtil {

    public static String parseMinMaxCelsius(Element day, String selector) {
        String dataContent = day.select(selector)
                .select("a")
                .attr("data-content");
        Document block = Jsoup.parse(dataContent);
        return block.select("div.ik.min-max-line")
                .select("span.ik.max-daily-temp")
                .text();
    }

}
