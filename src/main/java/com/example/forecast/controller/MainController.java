package com.example.forecast.controller;

import com.example.forecast.repository.ReportRepository;
import com.example.forecast.service.MainService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.Map;

@RestController
@RequestMapping(path = "api/v1/main")
public class MainController {

    private MainService mainService;
    private ReportRepository reportRepository;

    public MainController(MainService mainService, ReportRepository reportRepository) {
        this.mainService = mainService;
        this.reportRepository = reportRepository;
    }

    @GetMapping("")
    public String home() {
        return "<link rel='stylesheet' href='/stylesheet.css'>" +
                "<script src='/script.js'></script>" +
                "<h2>Weather Forecast API</h2>" +
                "<button class='glow-on-hover' onclick=relocateMax()>Max</button>" +
                "<button class='glow-on-hover' onclick=relocateMin()>Min</button>" +
                "<button class='glow-on-hover' onclick=relocateAll()>Both</button>" +
                "<br/><h2>Operations with database</h2>" +
                "<a href='/api/v1/main/database'>On /api/v1/main/database path</a>";
    }

    @GetMapping(path = "/max")
    public Map<LocalDate, Integer> getMax() {
        return mainService.getMaxTemperatures();
    }

    @GetMapping(path = "/min")
    public Map<LocalDate, Integer> getMin() {
        return mainService.getMinTemperatures();
    }

    @GetMapping(path = "/all")
    public Map<LocalDate, Map<String, Integer>> getAll() {
        return mainService.getAll();
    }

}
