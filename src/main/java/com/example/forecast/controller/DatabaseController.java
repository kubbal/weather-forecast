package com.example.forecast.controller;

import com.example.forecast.model.Report;
import com.example.forecast.repository.ReportRepository;
import com.example.forecast.service.MainService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * DATABASE RELATED API ENDPOINTS
 * Method   Path                                Action
 * POST     /api/v1/main/database/create        Saves new Report instance into the database
 * GET      /api/v1/main/database/reports       Gets all existing reports from the database
 * GET      /api/v1/main/database/report/:id    Retrieves a Report by its ID
 * PUT      /api/v1/main/database/report/:id    Updates a Report by its ID
 * DELETE   /api/v1/main/database/report/:id    Deletes a Report from database by its ID
 * DELETE   /api/v1/main/database/clear         Deletes all Reports from the database
 * GET      /api/v1/main/database/reports?reported=[date] Find all Reports that were created on the same day
 * GET      /api/v1/main/database/reports?date=[date] Find all Reports that were created for a specific day
 * GET      /api/v1/main/database/generate      Fills database with forecasts from forecast.url
 *
 * All endpoints were tested manually with Advanced Rest Client
 * */
@RestController
@RequestMapping(path = "api/v1/main/database")
public class DatabaseController {

    private ReportRepository reportRepository;
    private MainService mainService;

    public DatabaseController(ReportRepository reportRepository, MainService mainService) {
        this.reportRepository = reportRepository;
        this.mainService = mainService;
    }

    @GetMapping("")
    public String home() {
        return "<link rel='stylesheet' href='/stylesheet.css'>" +
                "<h2>Operations with database</h2>" +
                "<button class='glow-on-hover' onclick=relocateToGenerate()>Fill database</button>" +
                "<br/><h2>Weather Forecast API</h2>" +
                "<a href='/api/v1/main'>On /api/v1/main path</a>" +
                "<script src='/script.js'></script>";
    }

    @GetMapping(path = "/reports")
    public ResponseEntity<List<Report>> getAllReports(@RequestParam(required = false) String reported,
                                                      @RequestParam(required = false) String date) {
        try {
            List<Report> reports = new ArrayList<>();
            if (reported == null && date == null)
                reportRepository.findAll().forEach(reports::add);
            else if (reported != null && date == null) {
                String[] splitted = reported.split("-");
                LocalDate localDate = LocalDate.of(Integer.parseInt(splitted[0]),
                        Integer.parseInt(splitted[1]),
                        Integer.parseInt(splitted[2]));
                reportRepository.findReportsByReportedOn(localDate).forEach(reports::add);
            } else if (reported == null && date != null) {
                String[] splitted = date.split("-");
                LocalDate localDate = LocalDate.of(Integer.parseInt(splitted[0]),
                        Integer.parseInt(splitted[1]),
                        Integer.parseInt(splitted[2]));
                reportRepository.findReportsByDate(localDate).forEach(reports::add);
            } else {
                String[] reportSplit = reported.split("-");
                String[] dateSplit = date.split("-");
                LocalDate reportDate = LocalDate.of(Integer.parseInt(reportSplit[0]),
                        Integer.parseInt(reportSplit[1]),
                        Integer.parseInt(reportSplit[2]));
                LocalDate date1 = LocalDate.of(Integer.parseInt(dateSplit[0]),
                        Integer.parseInt(dateSplit[1]),
                        Integer.parseInt(dateSplit[2]));
                List<Report> copyOfRepository = reportRepository.findAll();
                copyOfRepository.stream()
                        .filter(repdate -> repdate.getReportedOn().isEqual(reportDate) && repdate.getDate().isEqual(date1))
                        .forEach(reports::add);
            }
            if (reports.isEmpty())
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);

            return new ResponseEntity<>(reports, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/report/{id}")
    public ResponseEntity<Report> getReportById(@PathVariable("id") long id) {
        Optional<Report> report = reportRepository.findById(id);
        return report.isPresent() ? new ResponseEntity<>(report.get(), HttpStatus.OK) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/report/{id}")
    public ResponseEntity<Report> updateReportById(@PathVariable("id") long id,
                                                   @RequestBody Report report) {
        Optional<Report> rep = reportRepository.findById(id);
        if (rep.isPresent()) {
            Report newRep = rep.get();
            newRep.setReportedOn(report.getReportedOn());
            newRep.setDate(report.getDate());
            newRep.setMaximum(report.getMaximum());
            newRep.setMinimum(report.getMinimum());
            return new ResponseEntity<>(reportRepository.save(newRep), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<Report> createReport(@RequestBody Report report) {
        try {
            Report newReport = reportRepository.save(new Report(report.getReportedOn(), report.getDate(), report.getMaximum(), report.getMinimum()));
            return new ResponseEntity<>(newReport, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/report/{id}")
    public ResponseEntity<Report> deleteReport(@PathVariable("id") long id) {
        try {
            reportRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/clear")
    public ResponseEntity<Report> clearRepository() {
        try {
            reportRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/generate")
    public ResponseEntity<List<Report>> generate() {
        try {
            List<Report> result = new ArrayList<>();
            Map<LocalDate, Map<String, Integer>> all = mainService.getAll();
            all.forEach((localDate, stringIntegerMap) -> {
                Report newRep = new Report(LocalDate.now(), localDate, stringIntegerMap.get("max"), stringIntegerMap.get("min"));
                result.add(newRep);
                reportRepository.save(newRep);
            });
            return new ResponseEntity<>(result, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
