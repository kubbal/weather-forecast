package com.example.forecast.repository;

import com.example.forecast.model.Report;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface ReportRepository extends JpaRepository<Report, Long> {
    List<Report> findReportsByReportedOn(LocalDate reportedOn);
    List<Report> findReportsByDate(LocalDate date);
}
