package com.example.forecast.parser;

import com.example.forecast.model.Pair;
import com.example.forecast.util.ParseUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

@Component
public class WeatherParser {

    @Autowired
    private Environment environment;

    private static final String maxKey = "max";
    private static final String minKey = "min";

    public static Pair<LocalDate, Integer> getMaxTemperature(Element day, int i) {
        String maxCelsius = ParseUtil.parseMinMaxCelsius(day, "div.ik.max");
        LocalDate date = LocalDate.now().plusDays(i);
        return new Pair(date, Integer.valueOf(maxCelsius.substring(0, maxCelsius.length()-2)));
    }

    public static Pair<LocalDate, Integer> getMinTemperature(Element day, int i) {
        String minCelsius = ParseUtil.parseMinMaxCelsius(day, "div.ik.min");
        LocalDate date = LocalDate.now().plusDays(i);
        return new Pair(date, Integer.valueOf(minCelsius.substring(0, minCelsius.length()-2)));
    }

    public Map<LocalDate, Integer> collectMaxTemperatures() {
        Map<LocalDate, Integer> result = new TreeMap<>();
        try {
            Document document = Jsoup.connect(environment.getProperty("forecast.url")).timeout(5000).get();
            Elements dayDivs = document.select("div.ik.dailyForecastCol");
            for (int i=0; i<dayDivs.size(); i++) {
                Pair<LocalDate, Integer> pair = getMaxTemperature(dayDivs.get(i), i);
                result.put(pair.getKey(), pair.getValue());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public Map<LocalDate, Integer> collectMinTemperatures() {
        Map<LocalDate, Integer> result = new TreeMap<>();
        try {
            Document document = Jsoup.connect(environment.getProperty("forecast.url")).timeout(5000).get();
            Elements dayDivs = document.select("div.ik.dailyForecastCol");
            for (int i=0; i<dayDivs.size(); i++) {
                Pair<LocalDate, Integer> pair = getMinTemperature(dayDivs.get(i), i);
                result.put(pair.getKey(), pair.getValue());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public Map<LocalDate, Map<String, Integer>> collectAll() {
        Map<LocalDate, Map<String, Integer>> result = new TreeMap<>();
        try {
            Document document = Jsoup.connect(environment.getProperty("forecast.url")).timeout(5000).get();
            Elements dayDivs = document.select("div.ik.dailyForecastCol");
            for (int i=0; i<dayDivs.size(); i++) {
                Pair<LocalDate, Integer> maxPair = getMaxTemperature(dayDivs.get(i), i);
                Pair<LocalDate, Integer> minPair = getMinTemperature(dayDivs.get(i), i);
                Map<String, Integer> minmax = new HashMap<>();
                minmax.put(maxKey, maxPair.getValue());
                minmax.put(minKey, minPair.getValue());
                result.put(maxPair.getKey(), minmax);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
